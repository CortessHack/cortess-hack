<?php

namespace cortess\parser;

class Parser implements ParserInterface
{

    public function process(string $url, string $tag) :array
    {
        $str = file_get_contents($url);
        preg_match_all('#<'.$tag.'>(.+?)</'.$tag.'>#su', $str, $res);
        return $res;
    }

}
$parser = new Parser();
$result = $parser->process('http://theory.phphtml.net', 'header');
var_dump($result);
